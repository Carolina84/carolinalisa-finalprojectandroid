package com.example.carolinalisa.carolinalisa_finalproject;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class SecondActivity extends AppCompatActivity {

    TextView editText_idProduct, editText_title, editText_shrtDescription, editText_price, editText_available, editText_description, editText_idCategory, editText_image ;
    Button btn_back;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        //Get Parameters
        Bundle b= getIntent().getExtras();
        //Integer idProduct= b.getInt("idProduct");
        String title= b.getString("title");
        //String shrtDescription= b.getString("shrtDescription");
        //Integer price= b.getInt("price");
        //Integer available= b.getInt("available");
        //String description= b.getString("description");
        //Integer idCategory= b.getInt("idCategory");
        //Integer image= b.getInt("image");


        //FindviewByIds
        //editText_idProduct = (TextView) findViewById(R.id.editText_idProduct);
        editText_title = (TextView) findViewById(R.id.editText_title);
        //editText_shrtDescription = (TextView) findViewById(R.id.editText_shrtDescription);
        //editText_price = (TextView) findViewById(R.id.editText_price);
        //editText_available = (TextView) findViewById(R.id.editText_available);
        //editText_description = (TextView) findViewById(R.id.editText_description);
        //editText_idCategory = (TextView) findViewById(R.id.editText_idCategory);
        //editText_image = (TextView) findViewById(R.id.editText_image);

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        //SET TEXT
        //editText_idProduct.setText(idProduct);
        editText_title.setText(title);
        //editText_shrtDescription.setText(shrtDescription);
        //editText_price.setText(price);
        //editText_available.setText(available);
       // editText_description.setText(description);
        //editText_idCategory.setText(idCategory);


        //BTN  back
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}

