package com.example.carolinalisa.carolinalisa_finalproject;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    public String jsonStr;
    private String TAG= MainActivity.class.getSimpleName();
    public ArrayList<Products> dataList = new ArrayList<>();
    public ListView list;
    private final Handler handler = new Handler();
    private Timer timer;
    public ListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //findViewByIds
        list = (ListView) findViewById(R.id.list);

        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            new GetProducts().execute("http://10.0.2.2/crud-php/api/products/getData.php");

                        } catch (Exception e) {
                            Log.d(TAG ,e.getMessage());
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 60000); //execute 1 minute


        new GetProducts().execute("http://10.0.2.2/crud-php/api/products/getData.php");


    }

    private boolean isConnected () {
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        boolean isMobileConn = networkInfo.isConnected();

        Log.d(TAG, "Wifi connected: " + isWifiConn);
        Log.d(TAG, "Mobile connected: " + isMobileConn);

        if (isWifiConn || isMobileConn) {
            return true;
        }else{
            return false;
        }

    }


    private class GetProducts extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute(){

            super.onPreExecute();

            //CLEAN LIST
            adapter = new ItemAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,dataList);
            dataList.clear();
            list.setAdapter(null);

            //IS CONNECT
            if(!isConnected()){
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }

        }

        @Override
        protected String doInBackground(String... arg) {

            if(isConnected()){
                // Making a request to url and getting response
                HttpHandler sh = new HttpHandler();
                jsonStr = sh.makeServiceCall(arg[0]);
            }else{

                String filename="products.json";
                StringBuffer stringBuffer = new StringBuffer();

                try {
                    BufferedReader inputReader = new BufferedReader(new InputStreamReader(openFileInput(filename)));
                    String inputString;

                    while ((inputString = inputReader.readLine()) != null) {
                        stringBuffer.append(inputString);
                    }

                    jsonStr = stringBuffer.toString();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (ParseJson())
                return "AsyncTask finished";
            else
                return "Error";
        }

        @Override
        protected void onPostExecute(String retString){
            Toast.makeText(MainActivity.this,retString,Toast.LENGTH_LONG).show();


            list.setAdapter(adapter);

            //change Json file only if it's connected
            if(isConnected()){
                WriteJsonInLocalStorage();
            }


            list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick (AdapterView<?> adapter, View view, final int position, long arg){

                   // Toast.makeText(getApplicationContext(), dataList.get(position).getTitle(), Toast.LENGTH_LONG).show();

                    //Intent
                    Intent i= new Intent (getApplicationContext(), SecondActivity.class );
                  //  i.putExtra("idProduct", dataList.get(position).getIdProduct());
                    i.putExtra("title", dataList.get(position).getTitle());
                   // i.putExtra("shrtDescription", dataList.get(position).getShrtDescription());
                   // i.putExtra("price", dataList.get(position).getPrice());
                   // i.putExtra("available", dataList.get(position).getAvailable());
                   // i.putExtra("description", dataList.get(position).getDescription());
                   // i.putExtra("idCategory", dataList.get(position).getIdCategory());
                   // i.putExtra("image", dataList.get(position).getImage());

                    startActivity(i);

                }

            });

            Toast.makeText(getApplicationContext(),"data has been refreshed",Toast.LENGTH_LONG).show();

        }

        public void WriteJsonInLocalStorage() {

            String filename="products.json";
            String data= jsonStr;

            FileOutputStream fos;

            try {
                fos = openFileOutput(filename, Context.MODE_PRIVATE);
                fos.write(data.getBytes());
                fos.close();

                Toast.makeText(getApplicationContext(), filename + " saved", Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                Log.d("Main_activity",e.getMessage());
            }

        }

        public Boolean ParseJson () {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                JSONArray data = jsonObj.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    JSONObject c = data.getJSONObject(i);

                    Integer idProduct = c.getInt("idProduct");
                    String title = c.getString("title");
                    String shortDescription = c.getString("shortDescription");
                    Integer price = c.getInt("price");
                    String description = c.getString("description");
                    Integer idCategory = c.getInt("idCategory");
                    String image = c.getString("image");


                    dataList.add(new Products(idProduct, title, shortDescription, price, description, idCategory, image));
                }

            } catch (final JSONException e) {
                Log.e(TAG, "Impossible to download the json file.");
                Toast.makeText(getApplicationContext(),"Impossible to download the json.",Toast.LENGTH_LONG).show();
                return false;
            }

            return true;
        }
    }


}














