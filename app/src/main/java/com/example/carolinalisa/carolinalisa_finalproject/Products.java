package com.example.carolinalisa.carolinalisa_finalproject;


public class Products {
    private Integer idProduct;
    private String title;
    private String shrtDescription;
    private Integer price;

    private String description;
    private Integer idCategory;
    private String image;


    //Constructor
    public Products(Integer idProduct, String title, String shrtDescription, Integer price, String description, Integer idCategory, String image) {
        this.idProduct = idProduct;
        this.title = title;
        this.shrtDescription = shrtDescription;
        this.price = price;
        this.description = description;
        this.idCategory = idCategory;
        this.image = image;
    }


    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShrtDescription() {
        return shrtDescription;
    }

    public void setShrtDescription(String shrtDescription) {
        this.shrtDescription = shrtDescription;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
