package com.example.carolinalisa.carolinalisa_finalproject;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;


public class ItemAdapter extends ArrayAdapter<Products> {

    private Context context;
    private ArrayList<Products> items;

    public ItemAdapter(Context context, int resource, ArrayList<Products> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item, parent, false);
        }

        //Set data into the view.
        TextView Title = (TextView) rowView.findViewById(R.id.Title);
        TextView price = (TextView) rowView.findViewById(R.id.Price);


        //get Item
        Products item = this.items.get(position);

        Title.setText(item.getTitle());
        price.setText(String.valueOf(item.getPrice()));

        return rowView;
    }
}